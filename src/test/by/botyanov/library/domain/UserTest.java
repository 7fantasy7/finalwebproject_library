package test.by.botyanov.library.domain;

import by.botyanov.library.domain.User;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserTest extends TestCase {
    private User user;

    @Before
    public void setUp() {
        user = new User();
    }

    @Test
    public void testGetLogin() {
        String result = user.getLogin();
        Assert.assertEquals(null, result);
    }
}