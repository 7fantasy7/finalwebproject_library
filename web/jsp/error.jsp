<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <title>Error ${pageContext.errorData.statusCode}</title>
    <link href="/css/style.css" rel="stylesheet">
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="header">
</div>
<div class="container">
    <section>
        <div id="left">
            <form action="${pageContext.request.contextPath}/jsp/welcome.jsp">
                <input type="submit" value="<fmt:message key="title.welcomepage"/>">
            </form>
        </div><br />

        <h1>Error</h1>
        <br/>
        <p>Status code: ${pageContext.errorData.statusCode}</p>
        <p id="reg">
            <c:if test="${role eq 'ADMIN'}">
                <a href="${pageContext.request.contextPath}/jsp/admin/welcome_admin.jsp"><fmt:message
                        key="error.backtowelcome"/></a>
            </c:if>
            <c:if test="${role eq 'USER'}">
                <a href="${pageContext.request.contextPath}/jsp/user/welcome_user.jsp"><fmt:message
                        key="error.backtowelcome"/></a>
            </c:if>
        </p>
    </section>
</div>
</body>
</html>