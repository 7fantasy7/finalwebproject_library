package by.botyanov.library.web.command;

import by.botyanov.library.dao.DAOException;
import by.botyanov.library.dao.OrderDao;
import by.botyanov.library.web.CommandException;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class SendOrderCommand implements Command {
    private static final Logger LOG = Logger.getLogger(SendOrderCommand.class);

    private SendOrderCommand() {
    }

    private static class AddBookToOrderCommandHolder {
        private final static SendOrderCommand instance = new SendOrderCommand();
    }

    public static SendOrderCommand getInstance() {
        return AddBookToOrderCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        long orderId = Long.parseLong(request.getParameter(ParamConstants.ORDER_ID));

        try {
            OrderDao.getInstance().sendOrder(orderId);
        } catch (DAOException e) {
            throw new CommandException(e);
        }

        return PageConstants.WELCOME_USER_PAGE;
    }
}