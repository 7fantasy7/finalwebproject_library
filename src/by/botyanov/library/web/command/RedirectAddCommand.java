package by.botyanov.library.web.command;

import by.botyanov.library.dao.AuthorDao;
import by.botyanov.library.dao.BookDao;
import by.botyanov.library.dao.DAOException;
import by.botyanov.library.domain.Author;
import by.botyanov.library.domain.Book;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

//TODO:useless to redirect, find some normal way
public class RedirectAddCommand implements Command {
    private static final Logger LOG = Logger.getLogger(RedirectAddCommand.class);

    private RedirectAddCommand() {
    }

    private static class RedirectAddCommandHolder {
        private final static RedirectAddCommand instance = new RedirectAddCommand();
    }

    public static RedirectAddCommand getInstance() {
        return RedirectAddCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        ArrayList<Author> foundAuthors;
        ArrayList<Book> foundBookNames;
        try {
            foundAuthors = AuthorDao.getInstance().getAllAuthors();
            foundBookNames = (ArrayList<Book>) BookDao.getInstance().findBooksLike("");
        } catch (DAOException e) {
            LOG.error("Error while finding authors or titles in database " + e);
            return PageConstants.ADD_BOOK_PAGE;
        }
        request.setAttribute(ParamConstants.AUTHORS_LIST, foundAuthors);
        for (Book b : foundBookNames) {
            LOG.debug(b);
        }
        request.setAttribute(ParamConstants.BOOK_LIST, foundBookNames);

        return PageConstants.ADD_BOOK_PAGE;
    }
}