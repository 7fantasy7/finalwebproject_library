package by.botyanov.library.web.command;

import by.botyanov.library.dao.DAOException;
import by.botyanov.library.dao.UserDao;
import by.botyanov.library.domain.User;
import by.botyanov.library.domain.UserRole;
import by.botyanov.library.web.CommandException;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class RedirectToEditUserCommand implements Command {
    private static final Logger LOG = Logger.getLogger(RedirectToEditUserCommand.class);

    private RedirectToEditUserCommand() {
    }

    private static class RedirectToEditBookCommandHolder {
        private final static RedirectToEditUserCommand instance = new RedirectToEditUserCommand();
    }

    public static RedirectToEditUserCommand getInstance() {
        return RedirectToEditBookCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String userLogin = request.getParameter(ParamConstants.USER_LOGIN_EDIT);
        ArrayList<UserRole> roles = new ArrayList<UserRole>() {{
            add(UserRole.USER);
            add(UserRole.ADMIN);
        }};
        User user;
        try {
            user = UserDao.getInstance().findByLogin(userLogin);
            LOG.debug(user);
        } catch (DAOException e) {
            LOG.debug(e.getMessage());
            throw new CommandException(e);
        }

        request.setAttribute(ParamConstants.USER, user);
        request.setAttribute(ParamConstants.USER_ROLES, roles);

        return PageConstants.EDIT_USER;
    }
}



