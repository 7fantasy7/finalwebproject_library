package by.botyanov.library.web.command.auth;

import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.command.Command;

import javax.servlet.http.HttpServletRequest;

public class LogoutCommand implements Command {

    private LogoutCommand() {
    }

    private static class LogoutCommandHolder {
        private final static LogoutCommand instance = new LogoutCommand();
    }

    public static LogoutCommand getInstance() {
        return LogoutCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().invalidate();
        return PageConstants.LOGIN_PAGE;
    }
}