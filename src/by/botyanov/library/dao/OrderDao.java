package by.botyanov.library.dao;

import by.botyanov.library.dao.pool.ConnectionPool;
import by.botyanov.library.dao.pool.ProxyConnection;
import by.botyanov.library.domain.Book;
import by.botyanov.library.domain.Order;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDao {
    private static final Logger LOG = Logger.getLogger(OrderDao.class);

    private static final String FIND_USER_ORDERS = "SELECT o.id,o.user_id,o.start,o.place,o.state,b.title FROM `order` o JOIN book b ON (b.order_id = o.id) WHERE user_id = ?;";
    private static final String FIND_USER_CURRENT_ORDER = "SELECT * FROM `order` WHERE state = \"PREPARING\" AND user_id = ?";

    private static final String MAKE_ORDER = "INSERT INTO `order` (user_id,place) VALUES (?, ?);";
    private static final String SELECT_LAST_ID = "SELECT LAST_INSERT_ID()";

    private static final String ADD_BOOK_TO_ORDER = "UPDATE book SET order_id = ? WHERE id = ?;";

    private static final String FETCH_NEW_ORDERS = "SELECT * FROM `order` WHERE state = \"POSTED\";";

    private static final String SEND_ORDER = "UPDATE `order` SET state = \"POSTED\" WHERE id = ?;";
    private static final String APPROVE_ORDER = "UPDATE `order` SET state = \"APPROVED\" WHERE id = ?;";
    private static final String DENY_ORDER = "UPDATE `order` SET state = \"DENIED\" WHERE id = ?;";

    private OrderDao() {
    }

    private static class OrderDaoHolder {
        private final static OrderDao instance = new OrderDao();
    }

    public static OrderDao getInstance() {
        return OrderDaoHolder.instance;
    }

    public List<Order> findUserOrders(long userId) throws DAOException {
        LOG.debug("OrderDao.findUserOrders()");
        ArrayList<Order> orders = new ArrayList<>();
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_ORDERS)) {
            preparedStatement.setLong(1, userId);
            LOG.debug(preparedStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            int rowCounter = 0;
            long currId = 0;
            long currUserId = 0;
            Order.Place currPlace = Order.Place.HOME;
            Order.State currState = Order.State.PREPARING;
            Timestamp currStamp = new Timestamp(10);
            ArrayList<Book> books = new ArrayList<>();
            while (resultSet.next()) {
                rowCounter++;
                if (currId == 0) {
                    currId = resultSet.getLong("id");
                    currUserId = resultSet.getLong("user_id");
                    currStamp = resultSet.getTimestamp("start");
                    currPlace = Order.Place.valueOf(resultSet.getString("place").toUpperCase());
                    currState = Order.State.valueOf(resultSet.getString("state").toUpperCase());
                    books.add(new Book(resultSet.getString("title")));
                } else if (currId == resultSet.getInt("id")) {
                    books.add(new Book(resultSet.getString("title")));
                } else {
                    orders.add(new Order(currId, currUserId, currStamp, currPlace, currState, books));
                    currId = resultSet.getLong("id");
                    currUserId = resultSet.getLong("user_id");
                    currStamp = resultSet.getTimestamp("start");
                    currPlace = Order.Place.valueOf(resultSet.getString("place").toUpperCase());
                    currState = Order.State.valueOf(resultSet.getString("state").toUpperCase());
                    books = new ArrayList<>();
                    books.add(new Book(resultSet.getString("title")));
                }
            }
            if (rowCounter > 0) {
                resultSet.last();
                orders.add(new Order(currId, currUserId, currStamp, currPlace, currState, books));
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException while OrderDao.findUserOrders()", e);
        }
        return orders;
    }


    public Order findUserCurrentOrder(long userId) throws DAOException {
        LOG.debug("OrderDao.findUserCurrentOrder()");
        Order currentOrder = null;
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_CURRENT_ORDER)) {
            preparedStatement.setLong(1, userId);
            LOG.debug(preparedStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                currentOrder = new Order();
                currentOrder.setId(resultSet.getLong("id"));
                currentOrder.setUserId(resultSet.getLong("user_id"));
                currentOrder.setStart(resultSet.getTimestamp("start"));
                currentOrder.setPlace(Order.Place.valueOf(resultSet.getString("place").toUpperCase()));
                currentOrder.setState(Order.State.valueOf(resultSet.getString("state").toUpperCase()));
//                currentOrder = new Order(resultSet.getInt("id"), resultSet.getInt("user_id"), resultSet.getTimestamp("start"),
//                        Order.Place.valueOf(resultSet.getString("place").toUpperCase()), Order.State.valueOf(resultSet.getString("state").toUpperCase()));
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException while OrderDao.findUserCurrentOrder()", e);
        }
        return currentOrder;
    }

    public List<Order> findAllOrders() throws DAOException {
        LOG.debug("OrderDao.findUserOrders()");
        ArrayList<Order> orders = new ArrayList<>();
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FETCH_NEW_ORDERS)) {
            LOG.debug(preparedStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Order order = new Order();
                order.setId(resultSet.getLong("id"));
                order.setUserId(resultSet.getLong("user_id"));
                order.setStart(resultSet.getTimestamp("start"));
                order.setPlace(Order.Place.valueOf(resultSet.getString("place").toUpperCase()));
                order.setState(Order.State.valueOf(resultSet.getString("state").toUpperCase()));
                orders.add(order);
//                orders.add(new Order(resultSet.getInt("id"), resultSet.getInt("userId"), resultSet.getTimestamp("start"),
//                        Order.Place.valueOf(resultSet.getString("place")), Order.State.valueOf(resultSet.getString("state"))));
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException while OrderDao.findAllOrders()", e);
        }
        return orders;
    }

    public long makeOrder(Order order) throws DAOException {
        LOG.debug("OrderDao.makeOrder()");
        long userId = order.getUserId();
        ArrayList<Book> books = order.getBooks();
        Order.Place place = order.getPlace();
        long orderId = 0;
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(MAKE_ORDER);
             Statement selectIdStatement = connection.createStatement();
             Statement foreignKeysCheck = connection.createStatement()) {
            for (Book book : books) {
                preparedStatement.setLong(1, userId);
                preparedStatement.setString(2, place.name().toLowerCase());
                LOG.debug(preparedStatement);
                foreignKeysCheck.executeQuery("SET FOREIGN_KEY_CHECKS=0");
                preparedStatement.executeUpdate();
                foreignKeysCheck.executeQuery("SET FOREIGN_KEY_CHECKS=1");
            }
            ResultSet orderIdSet = selectIdStatement.executeQuery(SELECT_LAST_ID);
            LOG.debug(orderIdSet);
            if (orderIdSet.next()) {
                orderId = orderIdSet.getInt("last_insert_id()");
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException while OrderDao.makeOrder()", e);
        }
        return orderId;
    }

    public void addBookToOrder(Order order, Book book) throws DAOException {
        LOG.debug("OrderDao.addBookToOrder()");
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_BOOK_TO_ORDER)) {
            preparedStatement.setLong(1, order.getId());
            preparedStatement.setLong(2, book.getId());
            preparedStatement.executeUpdate();
            LOG.debug(preparedStatement);
        } catch (SQLException e) {
            throw new DAOException("DAOException while OrderDao.addBookToOrder()", e);
        }
    }

    public void sendOrder(long orderId) throws DAOException {
        LOG.debug("OrderDao.sendOrder()");
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SEND_ORDER)) {
            preparedStatement.setLong(1, orderId);
            preparedStatement.executeUpdate();
            LOG.debug(preparedStatement);
        } catch (SQLException e) {
            throw new DAOException("DAOException while OrderDao.sendOrder()", e);
        }
    }

    public void approveOrder(int orderId, boolean flag) throws DAOException {
        LOG.debug("OrderDao.approveOrder()");
        String sql;
        sql = flag ? APPROVE_ORDER : DENY_ORDER;
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, orderId);
            preparedStatement.executeUpdate();
            LOG.debug(preparedStatement);
        } catch (SQLException e) {
            throw new DAOException("DAOException while OrderDao.approveOrder()", e);
        }
    }

}