package by.botyanov.library.web;

public class ParamConstants {
    private ParamConstants(){
    }
    public static final String USER_ID = "user_id";
    public static final String USER_LOGIN = "login";
    public static final String USER_PASSWORD = "password";
    public static final String USER_PASSWORD_REPEAT = "repassword";
    public static final String USER_FIRST_NAME = "firstname";
    public static final String USER_LAST_NAME = "lastname";
    public static final String USER_ROLE = "role";

    public static final String PARAM_LANGUAGE = "language";
    public static final String PARAM_LANGUAGE_PAGE = "page";
    public static final String PARAM_LOCALE = "locale";

    public static final String BOOK_AUTHOR = "author";
    public static final String BOOK_TITLE = "title";

    public static final String BOOK_SEARCH_PATTERN = "bookpattern";
    public static final String BOOKS_FOUND = "foundbooks";

    public static final String USER_LIST = "userlist";

    public static final String ORDER_REQUESTS = "orderrequests";
    public static final String APPROVE_ORDER = "approve";


    public static final String AUTHORS_LIST = "authorslist";
    public static final String BOOK_LIST = "booklist";
    public static final String ORDERS_LIST = "orderslist";

    public static final String BOOK = "book";
    public static final String BOOK_ID = "bookId";
    public static final String BOOK_DESCRIPTION = "description";

    public static final String ORDER_ID = "orderId";

    public static final String USER = "user";
    public static final String USER_ROLES = "userRoles";

    public static final String USER_LOGIN_EDIT = "userLogin";

}