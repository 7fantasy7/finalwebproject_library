package by.botyanov.library.service;

import by.botyanov.library.web.command.util.MD5;

import java.util.regex.Pattern;

public class DataValidator {
    private DataValidator() {
    }

    public static final String LOGIN_PASS_REGEX = "^[A-z0-9]{6,16}$";

    public static boolean validateRegisterData(String login, String password) {
        return loginValidation(login) && passwordValidation(password);
    }

    private static boolean loginValidation(String login) {
        return Pattern.matches(LOGIN_PASS_REGEX, login);
    }

    public static boolean passwordValidation(String password) {
        return Pattern.matches(LOGIN_PASS_REGEX, password);
    }

    public static boolean checkPass(String enteredPassword, String passwordFromBase) {
        return enteredPassword != null && !enteredPassword.isEmpty() && passwordFromBase != null &&
                !passwordFromBase.isEmpty() && MD5.md5(enteredPassword).equals(passwordFromBase);
    }
}
