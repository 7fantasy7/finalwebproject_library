package by.botyanov.library.web.command.auth;

import by.botyanov.library.dao.DAOException;
import by.botyanov.library.dao.UserDao;
import by.botyanov.library.domain.User;
import by.botyanov.library.domain.UserRole;
import by.botyanov.library.service.DataValidator;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import by.botyanov.library.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LoginCommand implements Command {
    private static final Logger LOG = Logger.getLogger(LoginCommand.class);

    private LoginCommand() {
    }

    private static class LoginCommandHolder {
        private final static LoginCommand instance = new LoginCommand();
    }

    public static LoginCommand getInstance() {
        return LoginCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        LOG.debug("Login started");
        HttpSession session = request.getSession();
        LOG.debug("DEB" + session.getAttribute(ParamConstants.USER_ROLE));

        if (session.getAttribute(ParamConstants.USER_ROLE) != null) {
            if (session.getAttribute(ParamConstants.USER_ROLE).equals(UserRole.ADMIN)) {
                return PageConstants.WELCOME_ADMIN_PAGE;
            } else {
                return PageConstants.WELCOME_USER_PAGE;
            }
        }
        String page;
        String login = request.getParameter(ParamConstants.USER_LOGIN);
        String password = request.getParameter(ParamConstants.USER_PASSWORD);
        User user;
        try {
            user = UserDao.getInstance().findByLogin(login);
        } catch (DAOException e) {
            LOG.error("Error while finding user in database " + e);
            return PageConstants.LOGIN_PAGE;
        }
        if (user == null) {
            request.setAttribute("errorLogin", true);
            return PageConstants.LOGIN_PAGE;
        }
        LOG.debug("Checking user password");
        if (DataValidator.checkPass(password, user.getPassword())) {
            session.setAttribute(ParamConstants.USER_ID, user.getId());
            session.setAttribute(ParamConstants.USER_LOGIN, user.getLogin());
            session.setAttribute(ParamConstants.USER_FIRST_NAME, user.getFirstName());
            session.setAttribute(ParamConstants.USER_LAST_NAME, user.getLastName());
            session.setAttribute(ParamConstants.USER_ROLE, user.getUserRole().toString().toUpperCase());
            if (user.getUserRole().equals(UserRole.ADMIN)) {
                page = PageConstants.WELCOME_ADMIN_PAGE;
            } else {
                page = PageConstants.WELCOME_USER_PAGE;
            }
        } else {
            request.setAttribute("errorPass", true);
            page = PageConstants.LOGIN_PAGE;

        }
        LOG.debug(page);
        return page;
    }
}