package by.botyanov.library.web.command.auth;

import by.botyanov.library.dao.DAOException;
import by.botyanov.library.dao.UserDao;
import by.botyanov.library.domain.User;
import by.botyanov.library.domain.UserRole;
import by.botyanov.library.service.DataValidator;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import by.botyanov.library.web.command.Command;
import by.botyanov.library.web.command.util.DataChecker;
import by.botyanov.library.web.command.util.MD5;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RegisterCommand implements Command {
    private static final Logger LOG = Logger.getLogger(RegisterCommand.class);

    private RegisterCommand() {
    }

    private static class RegisterCommandHolder {
        private final static RegisterCommand instance = new RegisterCommand();
    }

    public static RegisterCommand getInstance() {
        return RegisterCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String page = PageConstants.REGISTER_PAGE;
        String login = request.getParameter(ParamConstants.USER_LOGIN);
        String password = request.getParameter(ParamConstants.USER_PASSWORD);
        String passwordRepeat = request.getParameter(ParamConstants.USER_PASSWORD_REPEAT);
        String firstName = request.getParameter(ParamConstants.USER_FIRST_NAME);
        String lastName = request.getParameter(ParamConstants.USER_LAST_NAME);
        String role = String.valueOf(request.getSession().getAttribute(ParamConstants.USER_ROLE));

        LOG.debug(login + " " + password + " " + passwordRepeat + " " + firstName + " " + lastName + " " + role);

        if (login == null || firstName == null || lastName == null) {
            LOG.debug("Some of fields is empty");
            request.setAttribute("emptyFields", true);
            return page;
        }
        if (password == null || !password.equals(passwordRepeat)) {
            LOG.debug("Password Error");
            request.setAttribute("passwordError", true);
            return page;
        }
        if (!DataValidator.validateRegisterData(login, password)) {
            LOG.debug("RegEx Error");
            request.setAttribute("regexError", true);
            return page;
        }
        try {
            if (DataChecker.checkLoginExist(login)) {
                LOG.debug("Entered Login Exists");
                request.setAttribute("loginExists", true);
                return page;
            }
        } catch (DAOException e) {
            LOG.error("Error while checking login exist", e);
            request.setAttribute("errorRegistration", true);
            return page;
        }
        String hashPwd = MD5.md5(password);
        UserRole userRole = UserRole.USER;
        User user = new User(login, hashPwd, firstName, lastName, userRole);
        try {
            UserDao.getInstance().registerUser(user);
            page = PageConstants.WELCOME_USER_PAGE;
        } catch (DAOException e) {
            LOG.error("Error while saving user to database", e);
            request.setAttribute("errorRegistration", true);
            return page;
        }
        session.setAttribute(ParamConstants.USER_ID, user.getId());
        session.setAttribute(ParamConstants.USER_LOGIN, user.getLogin());
        session.setAttribute(ParamConstants.USER_FIRST_NAME, user.getFirstName());
        session.setAttribute(ParamConstants.USER_LAST_NAME, user.getLastName());
        session.setAttribute(ParamConstants.USER_ROLE, user.getUserRole().toString().toUpperCase());

        return page;
    }
}