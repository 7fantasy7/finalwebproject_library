package by.botyanov.library.web.command.util;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {
    private static final Logger LOG = Logger.getLogger(MD5.class);

    public static String md5(String target) {
        String code = null;
        try {
            byte[] data = MessageDigest.getInstance("MD5").digest(target.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (byte aData : data) {
                sb.append(Integer.toHexString((aData & 0xFF) | 0x100).substring(1, 3));
            }
            code = sb.toString();
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            LOG.error(e);
        }
        return code;
    }
}