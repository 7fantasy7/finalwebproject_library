package by.botyanov.library.web.command;

import by.botyanov.library.domain.Book;
import by.botyanov.library.service.BookService;
import by.botyanov.library.service.OrderService;
import by.botyanov.library.service.ServiceException;
import by.botyanov.library.web.CommandException;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class AddBookToOrderCommand implements Command {
    private static final Logger LOG = Logger.getLogger(AddBookToOrderCommand.class);

    private AddBookToOrderCommand() {
    }

    private static class AddBookToOrderCommandHolder {
        private final static AddBookToOrderCommand instance = new AddBookToOrderCommand();
    }

    public static AddBookToOrderCommand getInstance() {
        return AddBookToOrderCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        long bookId = Long.parseLong(request.getParameter(ParamConstants.BOOK_ID));
        long userId = (long) request.getSession().getAttribute(ParamConstants.USER_ID);

        try {
            Book book = BookService.find(bookId);
            OrderService.add(userId, book);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        return PageConstants.BOOK_RESULT;
    }
}