package by.botyanov.library.domain;

public enum UserRole {
    USER, ADMIN
}