<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <title>Registration</title>
    <link href="/css/style.css" rel="stylesheet">
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script>
        function checkForm() {
            if ((document.getElementById("pass1").value) != (document.getElementById("pass2").value)) {
                document.getElementById("msg").style.display = "inline";
            }
            else {
                document.getElementById("msg").style.display = "none";
            }
        }
    </script>
</head>
<body>
<div class="header">
</div>
<div class="container">
    <div id="top">
        <div id="lngform">
            <form name="LanguageForm" method="POST" action="${pageContext.request.contextPath}/controller">
                <input type="hidden" name="command" value="change_language"/>
                <input name="language" type="hidden" value="en_us"/>
                <input type="hidden" name="page" value="/jsp/login.jsp"/>
                <input type="image" class="flag" src="/images/uk_flag.jpg" alt="Submit">
            </form>
            <form name="LanguageForm" method="POST" action="${pageContext.request.contextPath}/controller">
                <input type="hidden" name="command" value="change_language"/>
                <input name="language" type="hidden" value="ru_ru"/>
                <input type="hidden" name="page" value="/jsp/login.jsp"/>
                <input type="image" class="flag" src="/images/ru_flag.jpg"
                       style="margin-right:10px;" alt="Submit">
            </form>
        </div>
    </div>

    <section>
        <form name="Registration" action="${pageContext.request.contextPath}/controller" method="POST">
            <input type="hidden" name="command" value="register"/>

            <div id="regexlogin"><fmt:message key="registration.loginpattern"/></div>
            <br/>
            <table>
                <tr>
                    <td><fmt:message key="registration.login"/>: <span class="red">*</span></td>
                    <td>
                        <input id="username" type="text" name="login" pattern="^[A-z0-9-_]{6,16}$" autofocus=""
                               required="">
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="registration.first_name"/>: <span class="red">*</span></td>
                    <td><input id="fname" type="text" name="firstname" pattern="^[A-zА-я-]{3,25}$" required="">
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="registration.last_name"/>: <span class="red">*</span></td>
                    <td>
                        <input id="lname" type="text" name="lastname" pattern="^[A-zА-я-]{3,25}$" required="">
                    </td>
                </tr>
            </table>
            <br/>

            <div id="regexpass"><fmt:message key="registration.passwordpattern"/></div>
            <br/>
            <table>
                <tr>
                    <td><fmt:message key="registration.password"/>:<span class="red">*</span></td>
                    <td>
                        <input id="pass1" type="password" name="password" pattern="^[a-zA-Z0-9-_]{6,16}$" autofocus=""
                               required="" onkeyup="checkForm(this)">
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="registration.re-password"/>:<span class="red">*</span></td>
                    <td><input id="pass2" type="password" name="repassword" required="" onkeyup="checkForm(this)"></td>
                </tr>
            </table>

            <div id="errormessage">
                <c:if test="${emptyFields == true}"><fmt:message key="message.info.emptyFields"/></c:if>
                <c:if test="${passwordError == true}"><fmt:message key="message.error.matchpass"/></c:if>
                <c:if test="${regexError == true}"><fmt:message key="message.error.regexmatch"/></c:if>
                <c:if test="${loginExists == true}"><fmt:message key="message.error.loginexists"/></c:if>
                <c:if test="${errorRegistration == true}"><fmt:message key="message.error.errorRegistration"/></c:if>
            </div>
            <div id="msg">
                <fmt:message key="message.error.matchpass"/>
            </div>
            <div>
                <input type="submit" value="<fmt:message key="registration.register"/>">
            </div>
        </form>
        <form name="backToLogin" action="${pageContext.request.contextPath}/controller" method="post">
            <div>
                <input type="hidden" name="command" value="back_to_login"/>
                <input type="submit" value="<fmt:message key="registration.backtologin"/>">
            </div>
        </form>
    </section>
</div>
</body>
</html>
