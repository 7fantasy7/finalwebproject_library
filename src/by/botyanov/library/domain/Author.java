package by.botyanov.library.domain;

import java.util.ArrayList;

public class Author extends Domain {
    private String fullName;
    private ArrayList<Book> books;

    public Author(long id, String fullName) {
        super(id);
        this.fullName = fullName;
    }

    public Author(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public ArrayList<Book> getBooks() {
        return new ArrayList<>(books);
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Author{");
        sb.append("id=").append(getId());
        sb.append(", fullName='").append(fullName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}