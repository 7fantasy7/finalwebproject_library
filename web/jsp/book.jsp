<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <title>Title</title>
    <link href="/css/style.css" rel="stylesheet">
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="header">
</div>
<div class="container">
    <div id="top">
        <div id="left">
            <form action="${pageContext.request.contextPath}/jsp/welcome.jsp">
                <input type="submit" value="<fmt:message key="title.welcomepage"/>">
            </form>
        </div>
        <%@include file="/jsp/jspf/lang.jspf" %>
    </div>

    <h1> Книга "<c:out value="${book.title}"/>"</h1>

    <p>ID <c:out value="${book.id}"/></p>

    <p>Автор(ы):
        <c:forEach items="${book.authors}" var="author" varStatus="loop">
            <c:out value="${author.fullName}"/><c:if test="${!loop.last}">, </c:if>
        </c:forEach></p>

    <p>Название: <c:out value="${book.title}"/></p>

    <p>Описание: <c:out value="${book.description}"/></p>
    <c:if test="${role == 'ADMIN'}">
        <form action="${pageContext.request.contextPath}/controller" method="POST">
            <input type="hidden" name="command" value="redirect_to_edit_book"/>
            <input type="hidden" name="bookId" value="${book.id}">
            <input type="submit" name="" value="" class="edit button"/>
        </form>
    </c:if>
</div>
</body>
</html>