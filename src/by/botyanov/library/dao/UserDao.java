package by.botyanov.library.dao;

import by.botyanov.library.dao.pool.ConnectionPool;
import by.botyanov.library.dao.pool.ProxyConnection;
import by.botyanov.library.domain.User;
import by.botyanov.library.domain.UserRole;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    private static final Logger LOG = Logger.getLogger(UserDao.class);

    private static final String REGISTER_USER = "INSERT INTO user (login, password, first_name, last_name, role) VALUES (?, ?, ?, ?, ?);";
    private static final String FETCH_ALL_USERS = "SELECT * FROM user;";
    private static final String FIND_USER_BY_LOGIN = "SELECT * FROM user WHERE login = ?;";
    private static final String CHANGE_PASSWORD = "UPDATE user SET password = ? WHERE login = ?;";

    private static final String UPDATE_USER_INFO = "UPDATE user SET login = ?, password = ?, first_name = ?, last_name = ?, role = ?, blocked = ? WHERE id = ?;";
    private static final String BLOCK_USER_TOGGLE = "UPDATE user SET blocked = !blocked WHERE login = ?";

    private UserDao() {
    }

    private static class UserDaoHolder {
        private final static UserDao instance = new UserDao();
    }

    public static UserDao getInstance() {
        return UserDaoHolder.instance;
    }

    public void registerUser(User user) throws DAOException {
        LOG.debug("UserDao.registerUser()");
        String login = user.getLogin();
        String password = user.getPassword();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String role = user.getUserRole().toString().toLowerCase();

        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(REGISTER_USER)) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            preparedStatement.setString(3, firstName);
            preparedStatement.setString(4, lastName);
            preparedStatement.setString(5, role);
            preparedStatement.executeUpdate();
            LOG.debug(preparedStatement);
            LOG.info("User " + login + " has been saved");
        } catch (SQLException e) {
            throw new DAOException("DAOException while UserDao.registerUser()", e);
        }
    }

    public List<User> findAll() throws DAOException {
        LOG.debug("UserDao.findByLogin()");
        List<User> users = new ArrayList<>();
        User user;
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(FETCH_ALL_USERS);
            while (resultSet.next()) {
                user = new User(resultSet.getString("login"), resultSet.getString("password"),
                        resultSet.getString("first_name"), resultSet.getString("last_name"),
                        UserRole.valueOf(resultSet.getString("role").toUpperCase()));
                user.setId(resultSet.getLong("id"));
                user.setblocked(resultSet.getBoolean("blocked"));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException while UserDao.findAll()", e);
        }
        return users;
    }

    public User findByLogin(String login) throws DAOException {
        LOG.debug("UserDao.findByLogin()");
        User user = null;
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User(resultSet.getString("login"), resultSet.getString("password"),
                        resultSet.getString("first_name"), resultSet.getString("last_name"),
                        UserRole.valueOf(resultSet.getString("role").toUpperCase()));
                user.setId(resultSet.getLong("id"));
                user.setblocked(resultSet.getBoolean("blocked"));
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException while UserDao.findByLogin()", e);
        }
        return user;
    }

    public void updateUserInfo(User newInfo) throws DAOException {
        LOG.debug("UserDao.updateUserInfo()");
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_INFO)) {
            preparedStatement.setString(1, newInfo.getLogin());
            preparedStatement.setString(2, newInfo.getPassword());
            preparedStatement.setString(3, newInfo.getFirstName());
            preparedStatement.setString(4, newInfo.getLastName());
            preparedStatement.setString(5, newInfo.getUserRole().toString());
            preparedStatement.setBoolean(6, newInfo.isblocked());
            preparedStatement.setLong(7, newInfo.getId());

            LOG.debug(preparedStatement.toString());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("DAOException while UserDao.updateUserInfo()", e);
        }
    }

    public void changePassword(String login, String newPassword) throws DAOException {
        LOG.debug("UserDao.changePassword()");
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(CHANGE_PASSWORD)) {
            preparedStatement.setString(1, newPassword);
            preparedStatement.setString(2, login);
            LOG.debug(preparedStatement.toString());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("DAOException while UserDao.changePassword()", e);
        }
    }

    public void toggleBlockUser(String login) throws DAOException {
        LOG.debug("UserDao.toggleBlockUser()");
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(BLOCK_USER_TOGGLE)) {
            preparedStatement.setString(1, login);
            LOG.debug(preparedStatement.toString());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("DAOException while UserDao.toggleBlockUser()", e);
        }
    }

}