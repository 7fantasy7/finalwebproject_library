<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <title>User List</title>
    <link href="/css/style.css" rel="stylesheet">
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="header">
</div>
<div class="container">
    <div id="top">
        <div id="left">
            <form action="${pageContext.request.contextPath}/jsp/welcome.jsp">
                <input type="submit" value="<fmt:message key="title.welcomepage"/>">
            </form>
        </div>
        <%@include file="/jsp/jspf/lang.jspf" %>
    </div>

    <c:if test="${!empty userlist}">
        <table border="2" width="700px" cellspacing="2" cellpadding="2">
            <thead>
            <tr>
                <th>ID</th>
                <th>Login</th>
                <th>First name</th>
                <th>Last name</th>
                <th>Role</th>
                <th>Blocked</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${userlist}" var="user">
                <tr>
                    <td>
                        <c:out value="${user.id}"/>
                    </td>
                    <td>
                        <c:out value="${user.login}"/>
                    </td>
                    <td>
                        <c:out value="${user.firstName}"/>
                    </td>
                    <td>
                        <c:out value="${user.lastName}"/>
                    </td>
                    <td>
                        <c:out value="${user.userRole}"/>
                    </td>
                    <td>
                        <c:out value="${user.blocked}"/>
                    </td>
                    <td>
                        <form action="${pageContext.request.contextPath}/controller" method="POST">
                            <input type="hidden" name="command" value="redirect_to_edit_user"/>
                            <input type="hidden" name="userLogin" value="${user.login}">
                            <input type="submit" name="" value="" class="edit button"/>
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

</div>
</body>
</html>
