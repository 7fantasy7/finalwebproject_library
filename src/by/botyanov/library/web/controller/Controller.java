package by.botyanov.library.web.controller;

import by.botyanov.library.dao.pool.ConnectionPool;
import by.botyanov.library.web.CommandException;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.command.Command;
import by.botyanov.library.web.command.factory.CommandFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(Controller.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        String log4jLocation = context.getRealPath(config.getInitParameter("log4j-init"));
        PropertyConfigurator.configure(log4jLocation);
        LOG.info("Servlet is started");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doAction(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doAction(req, resp);
    }


    private void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Command command = CommandFactory.getInstance().defineCommand(request);
        String page;
        try {
            page = command.execute(request);
        } catch (CommandException e) {
            LOG.error(e.getMessage());
            page = PageConstants.ERROR_PAGE;
            LOG.debug(page);
        }
        if (page != null) {
            request.getRequestDispatcher(page).forward(request, response);
        } else {
            page = PageConstants.INDEX_PAGE;
            response.sendRedirect(request.getContextPath() + page);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        ConnectionPool.getInstance().closeAllConnections();
    }
}