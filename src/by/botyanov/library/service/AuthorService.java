package by.botyanov.library.service;

import by.botyanov.library.dao.AuthorDao;
import by.botyanov.library.dao.DAOException;
import by.botyanov.library.domain.Author;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class AuthorService {
    private static final Logger LOG = Logger.getLogger(AuthorService.class);

    private AuthorService() {
    }

    public static List<Author> getAll() throws ServiceException {
        List<Author> list;
        try {
            list = AuthorDao.getInstance().getAllAuthors();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return list;
    }

    public static void add(ArrayList<Author> authors) throws ServiceException {
        try {
            AuthorDao.getInstance().addAuthors(authors);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}