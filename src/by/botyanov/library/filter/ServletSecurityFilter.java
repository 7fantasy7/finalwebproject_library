package by.botyanov.library.filter;

import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ServletSecurityFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(ServletSecurityFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        LOG.debug("Servlet security filtering");
        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();
        if (session.getAttribute(ParamConstants.USER_ROLE) == null && req.getParameter("command") != null
                && !req.getParameter("command").equalsIgnoreCase("login") &&
                !req.getParameter("command").equalsIgnoreCase("register") &&
                !req.getParameter("command").equalsIgnoreCase("change_language")) {
            request.getRequestDispatcher(PageConstants.INDEX_PAGE).forward(request, response);
            return;
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}