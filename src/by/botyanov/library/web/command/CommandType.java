package by.botyanov.library.web.command;

import by.botyanov.library.web.ApproveOrderCommand;
import by.botyanov.library.web.command.auth.LoginCommand;
import by.botyanov.library.web.command.auth.LogoutCommand;
import by.botyanov.library.web.command.auth.RegisterCommand;
import by.botyanov.library.web.command.common.LanguageCommand;

public enum CommandType {
    REGISTER (RegisterCommand.getInstance()),
    LOGIN(LoginCommand.getInstance()),
    LOGOUT(LogoutCommand.getInstance()),
    CHANGE_LANGUAGE(LanguageCommand.getInstance()),
    REDIRECT_TO_ADDING_BOOK(RedirectAddCommand.getInstance()),
    REDIRECT_TO_USER_LIST(RedirectUserListCommand.getInstance()),
    REDIRECT_TO_BOOK_LIST(RedirectBookListCommand.getInstance()),
    REDIRECT_TO_CART(RedirectToCartCommand.getInstance()),
    REDIRECT_TO_EDIT_BOOK(RedirectToEditBookCommand.getInstance()),
    REDIRECT_TO_EDIT_USER(RedirectToEditUserCommand.getInstance()),
    REDIRECT_TO_BOOK_REQUESTS(RedirectToBookRequestsCommand.getInstance()),
    EDIT_USER(EditUserCommand.getInstance()),
    SEND_ORDER(SendOrderCommand.getInstance()),
    EDIT_BOOK(EditBookCommand.getInstance()),
    BACK_TO_LOGIN (RedirectAddCommand.getInstance()),
    ADD_BOOK (AddBookCommand.getInstance()),
    SEARCH_BOOK (BookSearchCommand.getInstance()),
    OPEN_BOOK (OpenBookCommand.getInstance()),
    ORDER_BOOK (AddBookToOrderCommand.getInstance()),
    APPROVE_ORDER(ApproveOrderCommand.getInstance()),
    NULL(NullCommand.getInstance());

    Command command;

    CommandType(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }
}