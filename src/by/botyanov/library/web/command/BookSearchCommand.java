package by.botyanov.library.web.command;

import by.botyanov.library.domain.Book;
import by.botyanov.library.service.BookService;
import by.botyanov.library.service.ServiceException;
import by.botyanov.library.web.CommandException;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class BookSearchCommand implements Command {
    private static final Logger LOG = Logger.getLogger(BookSearchCommand.class);

    private BookSearchCommand() {
    }

    private static class BookSearchCommandHolder {
        private final static BookSearchCommand instance = new BookSearchCommand();
    }

    public static BookSearchCommand getInstance() {
        return BookSearchCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String pattern = request.getParameter(ParamConstants.BOOK_SEARCH_PATTERN);

        ArrayList<Book> foundBooks;
        try {
            foundBooks = (ArrayList<Book>) BookService.search(pattern);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        request.setAttribute(ParamConstants.BOOKS_FOUND, foundBooks);
        return PageConstants.BOOK_RESULT;
    }
}
