package by.botyanov.library.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import java.io.IOException;

public class CharacterEncodingFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(CharacterEncodingFilter.class);
    private static String code = "UTF-8";

    @Override
    public void init(FilterConfig config) throws ServletException {
        code = config.getInitParameter("encoding") != null ?
                config.getInitParameter("encoding") : "UTF-8";
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        LOG.debug("Character encoding filtering");
        String codeRequest = request.getCharacterEncoding();
        if (code != null && !code.equalsIgnoreCase(codeRequest)) {
            request.setCharacterEncoding(code);
            response.setCharacterEncoding(code);
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        code = null;
    }
}