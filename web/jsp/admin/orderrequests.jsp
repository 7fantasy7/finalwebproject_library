<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <title>Title</title>
    <link href="/css/style.css" rel="stylesheet">
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="header">
</div>
<div class="container">
    <div id="top">
        <div id="left">
            <form action="${pageContext.request.contextPath}/jsp/welcome.jsp">
                <input type="submit" value="<fmt:message key="title.welcomepage"/>">
            </form>
        </div>
        <%@include file="/jsp/jspf/lang.jspf"%>

        <c:if test="${login != null}">
            <form name="logOut" action="/controller" method="post" style="margin-left: 10px;float: right;">
                <input type="hidden" name="command" value="logout">
                <input type="submit" value="Logout ">
            </form>
            <p style="float: right;"> Hello, ${login}, (${role})</p>
        </c:if>
    </div>

    <c:if test="${!empty orderrequests}">
        <table border="2" width="2" cellspacing="2" cellpadding="2">
            <thead>
            <tr>
                <th>№</th>
                <th>Дата</th>
                <th>Место</th>
                <th>Состояние</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${orderrequests}" var="order">
                <tr>
                    <td>
                        <c:out value="${order.id}"/>
                    </td>
                    <td>
                        <c:out value="${order.start}"/>
                    </td>
                    <td>
                        <c:out value="${order.place}"/>
                    </td>
                    <td>
                        <c:out value="${order.state}"/>
                    </td>
                    <td>
                        <form action="${pageContext.request.contextPath}/controller" method="POST">
                            <input type="hidden" name="command" value="approve_order"/>
                            <input type="hidden" name="approve" value="true"/>
                            <input type="hidden" name="order_id" value="${order.id}">
                            <input type="submit" name="" value="Подтвердить"/>
                        </form>
                        <form action="${pageContext.request.contextPath}/controller" method="POST">
                            <input type="hidden" name="command" value="approve_order"/>
                            <input type="hidden" name="approve" value="false"/>
                            <input type="hidden" name="order_id" value="${order.id}">
                            <input type="submit" name="" value="Отклонить"/>
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    <c:if test="${empty orderrequests}">
        <p>No requests found </p>
    </c:if>
</div>
</body>
</html>
