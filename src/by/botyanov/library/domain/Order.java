package by.botyanov.library.domain;

import java.sql.Timestamp;
import java.util.ArrayList;

public class Order extends Domain {
    public enum Place {
        HOME, LIBRARY;

        public String getName() {
            return name();
        }
    }

    public enum State {
        PREPARING("PREPARING"), POSTED("POSTED"), APPROVED("APPROVED"), RETURNED("RETURNED"), CANCELLED("CANCELLED"), DENIED("DENIED");
        private final String val;

        State(String val) {
            this.val = val;
        }

        public String getState() {
            return name();
        }
    }

    private long userId;
    private ArrayList<Book> books;
    private Timestamp start;
    private Place place;
    private State state;

    public Order() {
    }

    public Order(long userId, ArrayList<Book> books, Place place) {
        this.userId = userId;
        this.books = books;
        this.place = place;
    }

    public Order(long id, long userId, Timestamp start, Place place, State state, ArrayList<Book> books) {
        super(id);
        this.userId = userId;
        this.start = start;
        this.place = place;
        this.state = state;
        this.books = books;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Order{");
        sb.append("id=").append(getId());
        sb.append(", userId=").append(userId);
        sb.append(", books=").append(books);
        sb.append(", start=").append(start);
        sb.append(", place=").append(place);
        sb.append(", state=").append(state);
        sb.append('}');
        return sb.toString();
    }
}
