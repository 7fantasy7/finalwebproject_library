package by.botyanov.library.filter;

import by.botyanov.library.domain.UserRole;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PageSecurityFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(PageSecurityFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        LOG.debug("Page security filtering");
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        UserRole role = UserRole.valueOf(httpRequest.getSession().getAttribute(ParamConstants.USER_ROLE).toString().toUpperCase());

        String returnPage;
        if (role == null) {
            returnPage = httpRequest.getContextPath() + PageConstants.INDEX_PAGE;
            httpResponse.sendRedirect(returnPage);
        } else {
            filterChain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}