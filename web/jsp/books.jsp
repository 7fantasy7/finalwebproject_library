<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <title>Title</title>
    <link href="/css/style.css" rel="stylesheet">
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="header">
</div>
<div class="container">
    <div id="top">
        <div id="left">
            <form action="${pageContext.request.contextPath}/jsp/welcome.jsp">
                <input type="submit" value="<fmt:message key="title.welcomepage"/>">
            </form>
        </div>
        <%@include file="/jsp/jspf/lang.jspf" %>
    </div>

    <form name="addNewBook" action="controller" method="POST">
        <input type="hidden" name="command" value="search_book"/>

        <div>
            <input type="text" name="bookpattern" placeholder="<fmt:message key="librarian.searchbook.title"/>*"/>
        </div>
        <input type="submit" value="<fmt:message key="librarian.searchbook.button"/>">
    </form>
    <c:if test="${!empty foundbooks}">
        <table border="2" width="700px" cellspacing="2" cellpadding="2">
            <thead>
            <tr>
                <th>Автор</th>
                <th>Название</th>
                <th>В корзину</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${foundbooks}" var="book">
                <tr>

                    <td>
                        <c:forEach items="${book.authors}" var="author" varStatus="loop">
                            <c:out value="${author.fullName}"/><c:if test="${!loop.last}">, </c:if>
                        </c:forEach>
                    </td>
                    <td>
                        <a href="?command=open_book&bookId=${book.id}"><c:out value="${book.title}"/></a>
                    </td>
                    <td>
                        <c:if test="${role == 'USER'}">
                            <form action="${pageContext.request.contextPath}/controller" method="POST">
                                <input type="hidden" name="command" value="order_book"/>
                                <input type="hidden" name="bookId" value="${book.id}">
                                <input type="submit" name="" value="" class="tobasket button"/>
                            </form>
                        </c:if>
                        <c:if test="${role == 'ADMIN'}">
                            <form action="${pageContext.request.contextPath}/controller" method="POST">
                                <input type="hidden" name="command" value="redirect_to_edit_book"/>
                                <input type="hidden" name="bookId" value="${book.id}">
                                <input type="submit" name="" value="" class="edit button"/>
                            </form>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

</div>
</body>
</html>
