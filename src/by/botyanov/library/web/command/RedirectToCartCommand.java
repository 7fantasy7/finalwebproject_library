package by.botyanov.library.web.command;

import by.botyanov.library.domain.Order;
import by.botyanov.library.service.OrderService;
import by.botyanov.library.service.ServiceException;
import by.botyanov.library.web.CommandException;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class RedirectToCartCommand implements Command {
    private static final Logger LOG = Logger.getLogger(RedirectToCartCommand.class);

    private RedirectToCartCommand() {
    }

    private static class RedirectToCartCommandHolder {
        private final static RedirectToCartCommand instance = new RedirectToCartCommand();
    }

    public static RedirectToCartCommand getInstance() {
        return RedirectToCartCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        ArrayList<Order> userOrders;

        try {
            userOrders = (ArrayList<Order>) OrderService.findAll
                    (Long.parseLong(String.valueOf(request.getSession().getAttribute(ParamConstants.USER_ID))));
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        request.setAttribute(ParamConstants.ORDERS_LIST, userOrders);

        return PageConstants.CART_PAGE;
    }
}
