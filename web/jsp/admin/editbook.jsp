<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <title>Edit Book</title>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <link href="/css/style.css" rel="stylesheet">

</head>
<body>
<div class="header">
</div>
<div class="container">
    <section>
        <div id="top">
            <div id="left">
                <form action="${pageContext.request.contextPath}/jsp/welcome.jsp">
                    <input type="submit" value="<fmt:message key="title.welcomepage"/>">
                </form>
            </div>
            <%@include file="/jsp/jspf/lang.jspf" %>

            <c:if test="${login != null}">
                <form name="logOut" action="/controller" method="post" style="margin-left: 10px;float: right;">
                    <input type="hidden" name="command" value="logout">
                    <input type="submit" value="Logout ">
                </form>
                <p style="float: right;"> Hello, ${login}, (${role})</p>
            </c:if>
        </div>
        <div><fmt:message key="librarian.editbook"/></div>
        <h1>${book.id}</h1>

        <form name="editBook" action="${pageContext.request.contextPath}/controller" method="POST">
            <input type="hidden" name="command" value="edit_book"/>
            <input type="hidden" name="bookId" value="${book.id}"/>
            <datalist id="author_list">
                <select name="authorz">
                    <c:forEach items="${authorslist}" var="authorq">
                        <option value="<c:out value="${authorq.fullName}"/>"></option>
                    </c:forEach>
                </select>
            </datalist>
            <div id="inputs" style="margin-bottom: 5px;">
                <c:forEach items="${book.authors}" var="author">
                    <input type="text" name="author" id="author" list="author_list" required
                           placeholder="<fmt:message key="librarian.addbook.author"/>*"
                           value="${author.fullName}" readonly/><br/>
                </c:forEach>
            </div>
            <div>
                <datalist id="title_list">
                    <select name="book">
                        <c:forEach items="${booklist}" var="book">
                            <option value="<c:out value="${book.title}"/>"></option>
                        </c:forEach>
                    </select>
                </datalist>
                <input type="text" name="title" id="title" list="title_list" required style="margin-bottom: 5px;"
                       placeholder="<fmt:message key="librarian.addbook.title"/>*" value="${book.title}"/>
                <br \>
                <textarea name="description" rows="10" cols="60" maxlength="240"
                          style="margin-bottom: 5px;">${book.description}</textarea>
            </div>
            <input type="submit" value="<fmt:message key="librarian.savebook"/>">
        </form>
        <div id="successmessage">
            <c:if test="${successadd == true}"><fmt:message key="message.info.successaddbook"/></c:if>
        </div>
        <div id="errormessage">
            <c:if test="${errorAddBook == true}"><fmt:message key="message.error.addBook"/></c:if>
        </div>
    </section>
</div>
</body>
</html>
