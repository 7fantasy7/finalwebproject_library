package by.botyanov.library.web.command;

import by.botyanov.library.dao.DAOException;
import by.botyanov.library.dao.OrderDao;
import by.botyanov.library.domain.Order;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class RedirectToBookRequestsCommand implements Command {
    private static final Logger LOG = Logger.getLogger(RedirectToBookRequestsCommand.class);

    private RedirectToBookRequestsCommand() {
    }

    private static class RedirectToBookRequestsCommandHolder {
        private final static RedirectToBookRequestsCommand instance = new RedirectToBookRequestsCommand();
    }

    public static RedirectToBookRequestsCommand getInstance() {
        return RedirectToBookRequestsCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        ArrayList<Order> orderRequests = new ArrayList<>();

        try {
            orderRequests = (ArrayList<Order>) OrderDao.getInstance().findAllOrders();
        } catch (DAOException e) {
            LOG.error("Error while finding user in database " + e);
        }
        request.setAttribute(ParamConstants.ORDER_REQUESTS, orderRequests);
        return PageConstants.REQUESTS_PAGE;
    }
}
