<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <title>Cart</title>
    <link href="/css/style.css" rel="stylesheet">
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="header">
</div>
<div class="container">
    <div id="top">
        <div id="left">
            <form action="${pageContext.request.contextPath}/jsp/welcome.jsp">
                <input type="submit" value="<fmt:message key="title.welcomepage"/>">
            </form>
        </div>
        <%@include file="/jsp/jspf/lang.jspf" %>
    </div>

    <h1>Заказы</h1>
    <c:if test="${!empty orderslist}">
        <table border="2" width="2" cellspacing="2" cellpadding="2">
            <thead>
            <tr>
                <th>№</th>
                <th>Дата</th>
                <th>Место</th>
                <th>Состояние</th>
                <th>Книги</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${orderslist}" var="order">
                <tr>
                    <td>
                        <c:out value="${order.id}"/>
                    </td>
                    <td>
                        <c:out value="${order.start}"/>
                    </td>
                    <td>
                        <c:out value="${order.place}"/>
                    </td>
                    <td>
                        <c:out value="${order.state}"/>
                    </td>
                    <td>
                        <c:forEach items="${order.books}" var="book" varStatus="loop">
                            <c:out value="${book.title}"/><c:if test="${!loop.last}">, </c:if>
                        </c:forEach>
                    </td>
                    <td>
                        <c:if test="${order.state eq 'PREPARING'}">
                            <form action="${pageContext.request.contextPath}/controller" method="POST">
                                <input type="hidden" name="command" value="send_order"/>
                                <input type="hidden" name="orderId" value="${order.id}">
                                <input type="submit" name="" value="" class="send button"/>
                            </form>

                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>

</body>
</html>
