<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <title>Add Book</title>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <link href="/css/style.css" rel="stylesheet">

    <script language="javascript">
        var add_counter = 1;
        function add_input() {
            if (add_counter++ < 8) {
                var new_input = document.createElement('div');
                new_input.innerHTML = '<input type="text" name="author" id=”author” list="author_list" required placeholder="<fmt:message key="librarian.addbook.author"/>*"><input type="button" class="authbtn" value="-" onclick="del_input(this.parentNode)">';
                document.getElementById('inputs').appendChild(new_input);
            } else {
                alert("<fmt:message key="librarian.addbook.nomoreauthors"/>");
            }
        }
        function del_input(obj) {
            add_counter--;
            document.getElementById('inputs').removeChild(obj)
        }
    </script>
</head>
<body>
<div class="header">
</div>
<div class="container">
    <div id="top">
        <div id="left">
            <form action="${pageContext.request.contextPath}/jsp/welcome.jsp">
                <input type="submit" value="<fmt:message key="title.welcomepage"/>">
            </form>
        </div>
        <%@include file="/jsp/jspf/lang.jspf" %>

        <c:if test="${login != null}">
            <form name="logOut" action="/controller" method="post" style="margin-left: 10px;float: right;">
                <input type="hidden" name="command" value="logout">
                <input type="submit" value="Logout ">
            </form>
            <p style="float: right;"> Hello, ${login}, (${role})</p>
        </c:if>
    </div>
    <section>
        <div><fmt:message key="librarian.addbook"/></div>

        <form name="addNewBook" action="${pageContext.request.contextPath}/controller" method="POST">
            <input type="hidden" name="command" value="add_book"/>
            <datalist id="author_list">
                <select name="country">
                    <c:forEach items="${authorslist}" var="author">
                        <option value="<c:out value="${author.fullName}"/>"></option>
                    </c:forEach>
                </select>
            </datalist>
            <div id="inputs" style="margin-bottom: 5px;">
                <input type="text" name="author" id="author" list="author_list" required
                       placeholder="<fmt:message key="librarian.addbook.author"/>*"/><input type="button"
                                                                                            class="authbtn" value="+"
                                                                                            onclick="add_input()">
            </div>
            <datalist id="title_list">
                <select name="country">
                    <c:forEach items="${booklist}" var="book">
                        <option value="<c:out value="${book.title}"/>"></option>
                    </c:forEach>
                </select>
            </datalist>
            <input type="text" name="title" id="title" list="title_list" required style="margin-bottom: 5px;"
                   placeholder="<fmt:message key="librarian.addbook.title"/>*"/><br/>
            <textarea name="description" rows="10" cols="60" maxlength="240" required
                      placeholder="<fmt:message key="librarian.addbook.description"/>"></textarea><br/>
            <input type="submit" value="<fmt:message key="librarian.addbook"/>">
        </form>
        <div id="successmessage">
            <c:if test="${successadd == true}"><fmt:message key="message.info.successaddbook"/></c:if>
        </div>
        <div id="errormessage">
            <c:if test="${errorAddBook == true}"><fmt:message key="message.error.addBook"/></c:if>
        </div>
    </section>
</div>
</body>
</html>
