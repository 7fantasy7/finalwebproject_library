package by.botyanov.library.web.command;

import by.botyanov.library.dao.BookDao;
import by.botyanov.library.dao.DAOException;
import by.botyanov.library.domain.Book;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class RedirectBookListCommand implements Command {
    private static final Logger LOG = Logger.getLogger(RedirectBookListCommand.class);

    private RedirectBookListCommand() {
    }

    private static class RedirectListCommandHolder {
        private final static RedirectBookListCommand instance = new RedirectBookListCommand();
    }

    public static RedirectBookListCommand getInstance() {
        return RedirectListCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String pattern = "";
        ArrayList<Book> foundBooks = new ArrayList<>();

        try {
            foundBooks = (ArrayList<Book>) BookDao.getInstance().findBooksLike(pattern);
        } catch (DAOException e) {
            LOG.error("Error while finding user in database " + e);
        }
        request.setAttribute(ParamConstants.BOOKS_FOUND, foundBooks);
        return PageConstants.BOOK_RESULT;
    }
}
