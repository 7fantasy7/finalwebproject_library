package by.botyanov.library.dao;

import by.botyanov.library.dao.pool.ConnectionPool;
import by.botyanov.library.dao.pool.ProxyConnection;
import by.botyanov.library.domain.Author;
import by.botyanov.library.domain.Book;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDao {
    private static final Logger LOG = Logger.getLogger(BookDao.class);

    private static final String FIND_BOOK_ID_BY_NAME = "SELECT id FROM book WHERE title = ?;";

    private static final String ADD_BOOK = "INSERT IGNORE INTO book (title,description) VALUES (?,?);";
    private static final String MAKE_BOOK_AUTHOR_RELATIONS = "INSERT INTO bookauthors (book_id,author_id) VALUES (?,?);";
    private static final String SELECT_LAST_ID = "SELECT LAST_INSERT_ID()";

    private static final String FIND_BOOKS_LIKE = "SELECT b.id, b.title, a.fullname FROM book b " +
            "JOIN bookauthors ba ON ( b.id = ba.book_id ) JOIN author a ON ( a.id = ba.author_id )" +
            "WHERE title LIKE ?" +
            "OR title IN (SELECT b1.title FROM book b1 JOIN bookauthors ba1 ON ( b1.id = ba1.book_id)" +
            "JOIN author a1 ON (a1.id = ba1.author_id) WHERE a1.fullname LIKE ?)" +
            "AND order_id=-1 GROUP BY title,fullname ORDER BY b.title ASC";

    private static final String DELETE_BOOK_BY_ID = "DELETE FROM book WHERE id=?";
    private static final String FIND_BOOK_BY_ID = "SELECT b.id, b.title, b.order_id, a.fullname, b.description FROM book b JOIN " +
            "bookauthors ba ON ( b.id = ba.book_id ) JOIN author a ON ( a.id = ba.author_id ) WHERE  b.id=?";

    private static final String UPDATE_BOOK_BY_ID = "UPDATE book SET title = ?, description = ? WHERE id = ?;";

    private BookDao() {
    }

    private static class BookDaoImplHolder {
        private final static BookDao instance = new BookDao();
    }

    public static BookDao getInstance() {
        return BookDaoImplHolder.instance;
    }

    public long addBook(Book book) throws DAOException {
        LOG.debug("BookDao.addBook()");
        long bookId = 0;
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_BOOK);
             Statement selectIdStatement = connection.createStatement()) {
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getDescription());
            preparedStatement.executeUpdate();

            ResultSet bookIdSet = selectIdStatement.executeQuery(SELECT_LAST_ID);
            LOG.debug(bookIdSet);
            if (bookIdSet.next()) {
                bookId = bookIdSet.getLong("last_insert_id()");
            }
            book.setId(bookId);
            LOG.debug(bookId);
            LOG.info("Book " + book + " has been saved");
            return bookId;
        } catch (SQLException e) {
            throw new DAOException("DAOException while BookDao.addBook()", e);
        }
    }

    public long findBookIdByName(String bookName) throws DAOException {
        LOG.info("BookDao.findBookIdByName()");
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_BOOK_ID_BY_NAME)) {
            preparedStatement.setString(1, bookName);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return result.getLong("id");
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException while BookDao.findBookIdByName()", e);
        }
        return -1;
    }

    public List<Book> findBooksLike(String pattern) throws DAOException {
        ArrayList<Book> bookList = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_BOOKS_LIKE)) {

            preparedStatement.setString(1, "%" + pattern + "%");
            preparedStatement.setString(2, "%" + pattern + "%");
            LOG.debug(preparedStatement.toString());
            ResultSet resultSet = preparedStatement.executeQuery();

            int currId = 0;
            String currTitle = "";
            ArrayList<Author> authors = new ArrayList<>();
            int rowCounter = 0;
            while (resultSet.next()) {
                rowCounter++;
                if (currId == 0) {
                    currId = resultSet.getInt("id");
                    currTitle = resultSet.getString("title");
                    authors.add(new Author(resultSet.getString("fullname")));
                } else if (currId == resultSet.getInt("id")) {
                    authors.add(new Author(resultSet.getString("fullname")));
                } else {
                    bookList.add(new Book(currId, currTitle, authors));
                    currTitle = resultSet.getString("title");
                    currId = resultSet.getInt("id");
                    authors = new ArrayList<>();
                    authors.add(new Author(resultSet.getString("fullname")));
                }
            }
            if (rowCounter > 0) {
                resultSet.last();
                bookList.add(new Book(resultSet.getInt("id"), resultSet.getString("title"), authors));
            }
            for (Book b : bookList) {
                LOG.debug(b);
            }
        } catch (SQLException e1) {
            LOG.error(e1.getMessage());
        }
        return bookList;
    }

    public void makeRelations(Book book, ArrayList<Author> authors) throws DAOException {
        LOG.debug("BookDao.makeRelations()");
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement relationStatement = connection.prepareStatement(MAKE_BOOK_AUTHOR_RELATIONS)) {
            long bookId = book.getId();
            for (Author author : authors) {
                relationStatement.setLong(1, bookId);
                relationStatement.setLong(2, author.getId());
                LOG.debug(relationStatement.toString());
                relationStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException while BookDao.makeRelations()", e);
        }
    }

    public void delete(long id) throws DAOException {
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BOOK_BY_ID)) {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DAOException("DAOException while BookDao.delete()", e);
        }
    }

    public Book find(long id) throws DAOException {
        Book book = new Book();
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_BOOK_BY_ID)) {
            preparedStatement.setLong(1, id);
            LOG.debug(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            int bookId = 0;
            String bookTitle = "";
            String description = "";
            long orderId = -1;
            ArrayList<Author> authors = new ArrayList<>();
            while (rs.next()) {
                if (bookId == 0) {
                    bookId = rs.getInt("id");
                    bookTitle = rs.getString("title");
                    description = rs.getString("description");
                    orderId = rs.getLong("order_id");
                    authors.add(new Author(rs.getString("fullname")));
                } else if (bookId == rs.getInt("id")) {
                    authors.add(new Author(rs.getString("fullname")));
                }
                book = new Book(bookId, bookTitle, description, authors, orderId);
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException while BookDao.find()", e);
        }
        return book;
    }

    public void updateTitleAndDesc(String title, String desc, long id) throws DAOException {
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_BOOK_BY_ID)) {
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, desc);
            preparedStatement.setLong(3, id);
            LOG.debug(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("DAOException while BookDao.updateTitleAndDesc()", e);
        }
    }
}