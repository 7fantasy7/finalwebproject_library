package by.botyanov.library.web.command;

import by.botyanov.library.web.PageConstants;

import javax.servlet.http.HttpServletRequest;

public class NullCommand implements Command {

    private NullCommand() {
    }

    private static class NullCommandHolder {
        private final static NullCommand instance = new NullCommand();
    }

    public static NullCommand getInstance() {
        return NullCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        return PageConstants.INDEX_PAGE;
    }
}
