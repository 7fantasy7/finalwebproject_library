package by.botyanov.library.web.command;

import by.botyanov.library.domain.Book;
import by.botyanov.library.service.BookService;
import by.botyanov.library.service.ServiceException;
import by.botyanov.library.web.CommandException;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class OpenBookCommand implements Command {
    private static final Logger LOG = Logger.getLogger(OpenBookCommand.class);

    private OpenBookCommand() {
    }

    private static class OpenBookCommandHolder {
        private final static OpenBookCommand instance = new OpenBookCommand();
    }

    public static OpenBookCommand getInstance() {
        return OpenBookCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        long bookId = Integer.parseInt(request.getParameter(ParamConstants.BOOK_ID));
        Book book;
        try {
            book = BookService.find(bookId);
            LOG.debug(book);
        } catch (ServiceException e) {
            LOG.debug(e.getMessage());
            throw new CommandException(e);
        }
        request.setAttribute(ParamConstants.BOOK, book);
        return PageConstants.BOOK_PAGE;
    }
}



