package by.botyanov.library.web.command.common;

import by.botyanov.library.web.ParamConstants;
import by.botyanov.library.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

public class LanguageCommand implements Command {

    private LanguageCommand() {
    }

    private static class LanguageCommandHolder {
        private final static LanguageCommand instance = new LanguageCommand();
    }

    public static LanguageCommand getInstance() {
        return LanguageCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String page = request.getParameter(ParamConstants.PARAM_LANGUAGE_PAGE);
        String language = request.getParameter(ParamConstants.PARAM_LANGUAGE);
        Locale locale = new Locale(language);
        session.setAttribute(ParamConstants.PARAM_LOCALE, locale);
        return page;
    }
}