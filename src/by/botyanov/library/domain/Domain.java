package by.botyanov.library.domain;

import java.io.Serializable;

public class Domain implements Serializable {
    private long id;

    public Domain() {
    }

    public Domain(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}