package by.botyanov.library.web.command;

import by.botyanov.library.web.CommandException;

import javax.servlet.http.HttpServletRequest;

public interface Command {
    String execute(HttpServletRequest request) throws CommandException;
}