<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <title>Login</title>
    <link href="/css/style.css" rel="stylesheet">
    <c:if test="${role != null}">
        <c:if test="${role eq 'ADMIN'}">
            <jsp:forward page="/jsp/admin/welcome_admin.jsp"/>
        </c:if>
        <c:if test="${role eq 'USER'}">
            <jsp:forward page="/jsp/user/welcome_user.jsp"/>
        </c:if>
    </c:if>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="header">
</div>
<div class="container">
    <section>
        <div id="top">
            <div id="lngform">
                <form name="LanguageForm" method="POST" action="${pageContext.request.contextPath}/controller">
                    <input type="hidden" name="command" value="change_language"/>
                    <input name="language" type="hidden" value="en_us"/>
                    <input type="hidden" name="page" value="/jsp/login.jsp"/>
                    <input type="image" class="flag" src="/images/uk_flag.jpg" alt="Submit">
                </form>
                <form name="LanguageForm" method="POST" action="${pageContext.request.contextPath}/controller">
                    <input type="hidden" name="command" value="change_language"/>
                    <input name="language" type="hidden" value="ru_ru"/>
                    <input type="hidden" name="page" value="/jsp/login.jsp"/>
                    <input type="image" class="flag" src="/images/ru_flag.jpg"
                           style="margin-right:10px;" alt="Submit">
                </form>
            </div>
        </div>

        <form name="LoginForm" action="${pageContext.request.contextPath}/controller" method="POST">
            <input type="hidden" name="command" value="login">

            <div>
                <input id="username" class="username" type="text"
                       placeholder="<fmt:message key="login.login"/>" required="" name="login"/>
            </div>
            <div>
                <input id="password" type="password" class="password"
                       placeholder="<fmt:message key="login.password"/>" required="" name="password"/>
            </div>
            <div>
                <input type="submit" name="login"
                       value="<fmt:message key="login.enter"/> "/>
            </div>
            <div id="errormessage">
                <c:if test="${errorLogin == true}"><fmt:message key="message.error.login"/></c:if>
                <c:if test="${errorPass == true}"><fmt:message key="message.error.pass"/></c:if>
            </div>
        </form>
        <div>
            <p id="reg">
                <a href="${pageContext.request.contextPath}/jsp/register.jsp"><fmt:message
                        key="login.registration"/></a>
            </p>

            <p id="forgotpass">
                <a href="${pageContext.request.contextPath}/jsp/forgotpassword.jsp"><fmt:message
                        key="login.forgotpassword"/></a>
            </p>
        </div>
    </section>
</div>
</body>
</html>
