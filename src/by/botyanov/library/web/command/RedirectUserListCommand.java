package by.botyanov.library.web.command;

import by.botyanov.library.dao.DAOException;
import by.botyanov.library.dao.UserDao;
import by.botyanov.library.domain.User;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class RedirectUserListCommand implements Command {
    private static final Logger LOG = Logger.getLogger(RedirectUserListCommand.class);

    private RedirectUserListCommand() {
    }

    private static class RedirectListCommandHolder {
        private final static RedirectUserListCommand instance = new RedirectUserListCommand();
    }

    public static RedirectUserListCommand getInstance() {
        return RedirectListCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        ArrayList<User> foundUsers = new ArrayList<>();

        try {
            foundUsers = (ArrayList<User>) UserDao.getInstance().findAll();
        } catch (DAOException e) {
            LOG.error("Error while finding user in database " + e);
        }
        for (User user : foundUsers){
            LOG.debug(user);
        }
        request.setAttribute(ParamConstants.USER_LIST, foundUsers);
        return PageConstants.USERS_RESULT;
    }
}
