package by.botyanov.library.web.command;

import by.botyanov.library.dao.BookDao;
import by.botyanov.library.dao.DAOException;
import by.botyanov.library.domain.Author;
import by.botyanov.library.domain.Book;
import by.botyanov.library.service.AuthorService;
import by.botyanov.library.service.BookService;
import by.botyanov.library.service.ServiceException;
import by.botyanov.library.web.CommandException;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AddBookCommand implements Command {
    private static final Logger LOG = Logger.getLogger(AddBookCommand.class);

    private AddBookCommand() {
    }

    private static class AddBookCommandHolder {
        private final static AddBookCommand instance = new AddBookCommand();
    }

    public static AddBookCommand getInstance() {
        return AddBookCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConstants.ADD_BOOK_PAGE;
        List<Author> collect = Stream.of(request.getParameterValues(ParamConstants.BOOK_AUTHOR))
                .map(Author::new).collect(Collectors.toList());
        ArrayList<Author> authors = new ArrayList<>(collect);

        String title = String.valueOf(request.getParameter(ParamConstants.BOOK_TITLE));
        String description = String.valueOf(request.getParameter(ParamConstants.BOOK_DESCRIPTION));

        try {
            AuthorService.add(authors);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        Book book = new Book(title, authors,description);

        try {
            BookService.add(book);

        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        try {
            BookDao.getInstance().makeRelations(book, authors);
            request.setAttribute("successadd", true);
        } catch (DAOException e) {
            request.setAttribute("errorAddBook", true);
            throw new CommandException(e);
        }
        return page;
    }
}