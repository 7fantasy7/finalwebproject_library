package by.botyanov.library.domain;

import java.util.ArrayList;

public class Book extends Domain {
    private String title;
    private String description;
    private ArrayList<Author> authors;
    private long orderId;

    public Book() {
    }

    public Book(String title) {
        this.title = title;
    }

    public Book(String title, ArrayList<Author> authors) {
        this.title = title;
        this.authors = authors;
    }

    public Book(String title, ArrayList<Author> authors, String description) {
        this.title = title;
        this.authors = authors;
        this.description = description;
    }


    public Book(String title, ArrayList<Author> authors, long orderId) {
        this.title = title;
        this.authors = authors;
        this.orderId = orderId;
    }

    public Book(long id, String title, ArrayList<Author> authors) {
        super(id);
        this.title = title;
        this.authors = authors;
    }

    public Book(long id, String title, ArrayList<Author> authors, long orderId) {
        super(id);
        this.title = title;
        this.authors = authors;
        this.orderId = orderId;
    }

    public Book(long id, String title, String description, ArrayList<Author> authors, long orderId) {
        super(id);
        this.title = title;
        this.description = description;
        this.authors = authors;
        this.orderId = orderId;
    }

    public ArrayList<Author> getAuthors() {
        return new ArrayList<>(authors);
    }

    public void setAuthors(ArrayList<Author> authors) {
        this.authors = authors;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Book{");
        sb.append("id=").append(getId());
        sb.append(", title='").append(title).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", authors=").append(authors);
        sb.append(", orderId=").append(orderId);
        sb.append('}');
        return sb.toString();
    }
}