package test.by.botyanov.library.service;

import by.botyanov.library.domain.User;
import by.botyanov.library.domain.UserRole;
import by.botyanov.library.service.DataValidator;
import junit.framework.TestCase;
import org.junit.Assert;

public class DataValidatorTest extends TestCase {
    User user = new User("User812", "1qaazxsw2","Афанасий", "Мамок", UserRole.USER);

    public void testValidateRegisterData() throws Exception {
        boolean result = DataValidator.validateRegisterData(user.getLogin(),user.getPassword());
        Assert.assertEquals(true, result);
    }
}