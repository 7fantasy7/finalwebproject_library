<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <c:if test="${role != null}">
        <c:if test="${role eq 'ADMIN'}">
            <jsp:forward page="/jsp/admin/welcome_admin.jsp"/>
        </c:if>
        <c:if test="${role eq 'USER'}">
            <jsp:forward page="/jsp/user/welcome_user.jsp"/>
        </c:if>
    </c:if>
</head>
<body>
</body>
</html>