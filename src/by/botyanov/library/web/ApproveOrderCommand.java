package by.botyanov.library.web;

import by.botyanov.library.dao.DAOException;
import by.botyanov.library.dao.OrderDao;
import by.botyanov.library.domain.Order;
import by.botyanov.library.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ApproveOrderCommand implements Command {
    private static final Logger LOG = Logger.getLogger(ApproveOrderCommand.class);

    private ApproveOrderCommand() {
    }

    private static class ApproveOrderCommandHolder {
        private final static ApproveOrderCommand instance = new ApproveOrderCommand();
    }

    public static ApproveOrderCommand getInstance() {
        return ApproveOrderCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        boolean approve = Boolean.parseBoolean(request.getParameter(ParamConstants.APPROVE_ORDER));
        List<Order> allOrders;

        int orderId = Integer.parseInt(request.getParameter("order_id"));
        try {
            OrderDao.getInstance().approveOrder(orderId, approve);
        } catch (DAOException e) {
            LOG.debug(e.getMessage());
            throw new CommandException(e);
        }

        try {
            allOrders = OrderDao.getInstance().findAllOrders();
        } catch (DAOException e) {
            LOG.debug(e.getMessage());
            throw new CommandException(e);
        }

        request.setAttribute(ParamConstants.ORDER_REQUESTS, allOrders);
        return PageConstants.REQUESTS_PAGE;
    }
}