package by.botyanov.library.service;

import by.botyanov.library.dao.BookDao;
import by.botyanov.library.dao.DAOException;
import by.botyanov.library.domain.Book;
import org.apache.log4j.Logger;

import java.util.List;

public class BookService {
    private static final Logger LOG = Logger.getLogger(BookService.class);

    private BookService() {
    }

    public static void update(String title, String description, long bookId) throws ServiceException {
        try {
            BookDao.getInstance().updateTitleAndDesc(title, description, bookId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public static List<Book> search(String pattern) throws ServiceException {
        List<Book> list;
        try {
            list = BookDao.getInstance().findBooksLike(pattern);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return list;
    }

    public static Book find(long id) throws ServiceException {
        Book book;
        try {
            book = BookDao.getInstance().find(id);
        } catch (DAOException e) {
            LOG.debug(e.getMessage());
            throw new ServiceException(e);
        }
        return book;
    }

    public static void add(Book book) throws ServiceException {
        try {
            BookDao.getInstance().addBook(book);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}