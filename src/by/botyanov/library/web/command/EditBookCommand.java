package by.botyanov.library.web.command;

import by.botyanov.library.service.BookService;
import by.botyanov.library.service.ServiceException;
import by.botyanov.library.web.CommandException;
import by.botyanov.library.web.PageConstants;

import javax.servlet.http.HttpServletRequest;

public class EditBookCommand implements Command {
    private EditBookCommand() {
    }

    private static class EditBookCommandHolder {
        private final static EditBookCommand instance = new EditBookCommand();
    }

    public static EditBookCommand getInstance() {
        return EditBookCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        long bookId = Long.parseLong(request.getParameter("bookId"));
        String title = request.getParameter("title");
        String description = request.getParameter("description");

        try {
            BookService.update(title,description,bookId);
        } catch (ServiceException e) {
           throw new CommandException(e);
        }
        return PageConstants.BOOK_RESULT;
    }
}