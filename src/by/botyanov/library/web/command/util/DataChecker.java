package by.botyanov.library.web.command.util;

import by.botyanov.library.dao.DAOException;
import by.botyanov.library.dao.UserDao;
import by.botyanov.library.domain.User;

public class DataChecker {

    public static boolean checkLoginExist(String login) throws DAOException {
        User user = UserDao.getInstance().findByLogin(login);
        return user != null;
    }
}