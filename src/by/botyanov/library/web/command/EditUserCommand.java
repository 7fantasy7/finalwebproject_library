package by.botyanov.library.web.command;

import by.botyanov.library.dao.DAOException;
import by.botyanov.library.dao.UserDao;
import by.botyanov.library.domain.User;
import by.botyanov.library.domain.UserRole;
import by.botyanov.library.web.CommandException;
import by.botyanov.library.web.PageConstants;

import javax.servlet.http.HttpServletRequest;

public class EditUserCommand implements Command {
    private EditUserCommand() {
    }

    private static class EditBookCommandHolder {
        private final static EditUserCommand instance = new EditUserCommand();
    }

    public static EditUserCommand getInstance() {
        return EditBookCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        long userId = Long.parseLong(request.getParameter("userId"));
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String role = request.getParameter("userRole");
        boolean blocked = Boolean.valueOf(request.getParameter("blocked"));

        User newUser = new User(userId,login,password,firstName,lastName, UserRole.valueOf(role),blocked);
        try {
            UserDao.getInstance().updateUserInfo(newUser);
        } catch (DAOException e) {
           throw new CommandException(e);
        }

        return PageConstants.USERS_RESULT;
    }
}