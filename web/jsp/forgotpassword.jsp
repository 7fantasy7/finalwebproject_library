<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <title>ForgotPassword</title>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <link href="/css/style.css" rel="stylesheet">
</head>
<body>
<div class="header">
</div>
<div class="container">
    <section id="content">
        <div id="top">
            <div id = "left">
                <form action="${pageContext.request.contextPath}/jsp/welcome.jsp">
                    <input type="submit" value="<fmt:message key="title.welcomepage"/>">
                </form>
            </div>
            <%@include file="/jsp/jspf/lang.jspf"%>
        </div>

        <div id="forgotpasstext"><b><fmt:message key="message.info.forgotpassword"/></b></div>
        <br/>

        <div id="email">
            <a href="mailto:7fantasy7@gmail.com">7fantasy7@gmail.com</a>
        </div>
        <br />
        <a href="${pageContext.request.contextPath}/jsp/login.jsp"><fmt:message
                key="login.backtologin"/></a>

    </section>
</div>
</body>
</html>
