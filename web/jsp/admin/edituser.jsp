<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <title>Edit User</title>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <link href="/css/style.css" rel="stylesheet">
</head>
<body>
<div class="header">
</div>
<div class="container">
    <section>
        <div id="top">
            <div id="left">
                <form action="${pageContext.request.contextPath}/jsp/welcome.jsp">
                    <input type="submit" value="<fmt:message key="title.welcomepage"/>">
                </form>
            </div>
            <%@include file="/jsp/jspf/lang.jspf" %>

            <c:if test="${login != null}">
                <form name="logOut" action="/controller" method="post" style="margin-left: 10px;float: right;">
                    <input type="hidden" name="command" value="logout">
                    <input type="submit" value="Logout ">
                </form>
                <p style="float: right;"> Hello, ${login}, (${role})</p>
            </c:if>
        </div>

        <div><fmt:message key="librarian.edituser"/></div>

        <form name="editUser" action="${pageContext.request.contextPath}/controller" method="POST">
            <input type="hidden" name="command" value="edit_user"/>
            <input type="hidden" name="userId" value="${user.id}"/>


            <table>
                <tr>
                    <td><fmt:message key="admin.edituser.id"/>:</td>
                    <td>
                        <input type="text" name="id" id="id" required style="margin-bottom:5px;" readonly
                               value="${user.id}"/><br/>
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="admin.edituser.login"/>:</td>
                    <td><input type="text" name="login" id="login" required style="margin-bottom:5px;"
                               value="${user.login}"/><br/>
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="admin.edituser.password"/>:</td>
                    <td>
                        <input type="text" name="password" id="password" required
                               style="margin-bottom:5px;"
                               value="${user.password}"/><br/>
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="admin.edituser.firstname"/>:</td>
                    <td>
                        <input type="text" name="firstName" id="firstName" required style="margin-bottom:5px;"
                               value="${user.firstName}"/><br/>
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="admin.edituser.lastname"/>:</td>
                    <td>
                        <input type="text" name="lastName" id="lastName" required style="margin-bottom:5px;"
                               value="${user.lastName}"/><br/>
                    </td>
                </tr>
            </table>

            <select name="userRole">
                <c:forEach items="${userRoles}" var="role">
                    <option value="${role}" ${user.userRole == role ? 'selected' : ''}>${role}</option>
                </c:forEach>
            </select><br/>
            <c:choose>
                <c:when test="${user.blocked==true}">
                    <input type="checkbox" name="blocked" id="blocked" value="${user.blocked}" checked/><br/>
                </c:when>
                <c:otherwise>
                    <input type="checkbox" name="blocked" id="blocked" value="${user.blocked}"/>
                </c:otherwise>
            </c:choose>

            <label for="blocked">Blocked</label><br/>
            <input type="submit" value="<fmt:message key="librarian.saveuser"/>">
        </form>
        <div id="successmessage">
            <c:if test="${successadd == true}"><fmt:message key="message.info.successaddbook"/></c:if>
        </div>
        <div id="errormessage">
            <c:if test="${errorAddBook == true}"><fmt:message key="message.error.addBook"/></c:if>
        </div>
    </section>
</div>
</body>
</html>
