package test.by.botyanov.library.dao.pool;

import junit.framework.TestCase;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ConnectionPoolTest extends TestCase {
    private String host, dbName, username, password;

    public void setUp() throws Exception {
        super.setUp();
        ResourceBundle prop = ResourceBundle.getBundle("resources.db");

        host = prop.getString("db.host");
        dbName = prop.getString("db.name");
        username = prop.getString("db.username");
        password = prop.getString("db.password");
        DriverManager.registerDriver(new com.mysql.jdbc.Driver());
    }

    @Test
    public void testTakeConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(host + dbName, username, password);
        assertNotNull("Cant get connection", connection);
    }
}