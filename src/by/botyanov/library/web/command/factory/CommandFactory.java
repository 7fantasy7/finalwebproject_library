package by.botyanov.library.web.command.factory;

import by.botyanov.library.web.command.Command;
import by.botyanov.library.web.command.CommandType;
import by.botyanov.library.web.command.NullCommand;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class CommandFactory {
    private static final Logger LOG = Logger.getLogger(CommandFactory.class);
    private static AtomicBoolean instanceCreated = new AtomicBoolean();
    private static CommandFactory instance = null;
    private static ReentrantLock lock = new ReentrantLock();

    private CommandFactory() {
    }

    public static CommandFactory getInstance() {
        if (!instanceCreated.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new CommandFactory();
                    instanceCreated = new AtomicBoolean(true);
                    LOG.info("Instance of Command Factory created");
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public Command defineCommand(HttpServletRequest request) {
        String reqCommand = request.getParameter("command");
        Command command = NullCommand.getInstance();
        if (reqCommand != null && !reqCommand.isEmpty()) {
            CommandType commandType = CommandType.valueOf(reqCommand.toUpperCase());
            command = commandType.getCommand();
        }
        return command;
    }
}