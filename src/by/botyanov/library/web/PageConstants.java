package by.botyanov.library.web;

public class PageConstants {
    private PageConstants() {
    }

    public static final String INDEX_PAGE = "/index.jsp";
    public static final String REGISTER_PAGE = "/jsp/register.jsp";
    public static final String LOGIN_PAGE = "/jsp/login.jsp";
    public static final String ERROR_PAGE = "/jsp/error.jsp";
    public static final String WELCOME_USER_PAGE = "/jsp/user/welcome_user.jsp";
    public static final String WELCOME_ADMIN_PAGE = "/jsp/admin/welcome_admin.jsp";

    public static final String ADD_BOOK_PAGE = "/jsp/admin/addbook.jsp";

    public static final String USERS_RESULT = "/jsp/admin/userlist.jsp";

    public static final String BOOK_RESULT = "/jsp/books.jsp";
    public static final String REQUESTS_PAGE = "/jsp/admin/orderrequests.jsp";

    public static final String BOOK_PAGE = "/jsp/book.jsp";
    public static final String EDIT_BOOK = "/jsp/admin/editbook.jsp";

    public static final String EDIT_USER = "/jsp/admin/edituser.jsp";

    public static final String CART_PAGE = "/jsp/cart.jsp";

}