<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="resources.pagecontent"/>
    <title><fmt:message key="user.welcome.title"/></title>
    <link href="/css/style.css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="header">
</div>
<div class="container">
    <section id="content">
        <div id="top">
            <div id="left">
                <form action="${pageContext.request.contextPath}/jsp/welcome.jsp">
                    <input type="submit" value="<fmt:message key="title.welcomepage"/>">
                </form>
            </div>
            <%@include file="/jsp/jspf/lang.jspf" %>
            <c:if test="${login != null}">
                <form name="logOut" action="/controller" method="post" style="margin-left: 10px;float: right;">
                    <input type="hidden" name="command" value="logout">
                    <input type="submit" value="Logout ">
                </form>
                <p style="float: right;"> Hello, ${login}, (${role})</p>
            </c:if>
        </div>

        <div>
            <form name="addBook" action="${pageContext.request.contextPath}/controller" method="post">
                <input type="hidden" name="command" value="redirect_to_book_list"/>
                <input type="submit" value="<fmt:message key="user.searchbook"/>">
            </form>
        </div>
        <div>
            <form name="addBook" action="${pageContext.request.contextPath}/controller" method="post">
                <input type="hidden" name="command" value="redirect_to_cart"/>
                <input type="submit" value="<fmt:message key="user.cart"/>">
            </form>
        </div>
    </section>
</div>
</body>
</html>