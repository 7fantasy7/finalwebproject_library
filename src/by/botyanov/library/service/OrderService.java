package by.botyanov.library.service;

import by.botyanov.library.dao.DAOException;
import by.botyanov.library.dao.OrderDao;
import by.botyanov.library.domain.Book;
import by.botyanov.library.domain.Order;

import java.util.ArrayList;
import java.util.List;

public class OrderService {
    private OrderService() {
    }

    public static void add(long userId, Book book) throws ServiceException {
        Order currOrder;
        long ordId;
        try {
            currOrder = OrderDao.getInstance().findUserCurrentOrder(userId);
            ArrayList<Book> books = new ArrayList<>();
            books.add(book);
            if (currOrder == null) {
                currOrder = new Order(userId, books, Order.Place.HOME);
                ordId = OrderDao.getInstance().makeOrder(currOrder);
                currOrder.setId(ordId);
            }
            OrderDao.getInstance().addBookToOrder(currOrder, books.get(0));
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public static Order find(long userId) throws ServiceException {
        Order order;
        try {
            order = OrderDao.getInstance().findUserCurrentOrder(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return order;
    }

    public static List<Order> findAll(long userId) throws ServiceException {
        List<Order> orders;
        try {
            orders = OrderDao.getInstance().findUserOrders(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return orders;
    }
}
