package by.botyanov.library.dao;

import by.botyanov.library.dao.pool.ConnectionPool;
import by.botyanov.library.dao.pool.ProxyConnection;
import by.botyanov.library.domain.Author;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class AuthorDao {
    private static final Logger LOG = Logger.getLogger(AuthorDao.class);

    private static final String ADD_AUTHOR = "INSERT IGNORE INTO author (fullname) VALUES (?);";
    private static final String FETCH_ALL_AUTHORS = "SELECT * FROM author";
    private static final String FIND_AUTHOR_BY_NAME = "SELECT * FROM author WHERE fullname = ?;";
    private static final String SELECT_LAST_ID = "SELECT LAST_INSERT_ID()";

    private AuthorDao() {
    }

    private static class AuthorDaoHolder {
        private final static AuthorDao instance = new AuthorDao();
    }

    public static AuthorDao getInstance() {
        return AuthorDaoHolder.instance;
    }

    public void addAuthors(ArrayList<Author> authors) throws DAOException {
        LOG.debug("AuthorDao.addAuthor()");
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             Statement selectIdStatement = connection.createStatement();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_AUTHOR)) {
            for (Author author : authors) {
                String authorName = author.getFullName();
                preparedStatement.setString(1, authorName);
                LOG.debug(preparedStatement.toString());
                preparedStatement.executeUpdate();

                long authorId = -1;
                ResultSet bookIdSet = selectIdStatement.executeQuery(SELECT_LAST_ID);
                LOG.debug(bookIdSet);
                if (bookIdSet.next()) {
                    authorId = bookIdSet.getLong("last_insert_id()");
                }
                author.setId(authorId);
            }
            LOG.debug("Authors has been saved");
        } catch (SQLException e) {
            throw new DAOException("DAOException while BookDao.addAuthor()", e);
        }
    }

    public ArrayList<Author> getAllAuthors() throws DAOException {
        ArrayList<Author> authors = new ArrayList<>();
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             Statement statement = connection.createStatement()) {
            ResultSet authorsSet = statement.executeQuery(FETCH_ALL_AUTHORS);
            LOG.debug(authorsSet);
            while (authorsSet.next()) {
                authors.add(new Author(authorsSet.getLong("id"), authorsSet.getString("fullname")));
            }
            LOG.debug("Got all authors from base");
        } catch (SQLException e) {
            throw new DAOException("DAOException while BookDao.addAuthor()", e);
        }
        return authors;
    }

    public int findAuthorIdByName(String authorName) throws DAOException {
        LOG.info("BookDao.findAuthorIdByName()");
        try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_AUTHOR_BY_NAME)) {
            preparedStatement.setString(1, authorName);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return result.getInt("id");
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException while BookDao.findAuthorIdByName()", e);
        }
        return -1;
    }

    void delete(long id) {

    }

}