package by.botyanov.library.web.command;

import by.botyanov.library.domain.Author;
import by.botyanov.library.domain.Book;
import by.botyanov.library.service.AuthorService;
import by.botyanov.library.service.BookService;
import by.botyanov.library.service.ServiceException;
import by.botyanov.library.web.CommandException;
import by.botyanov.library.web.PageConstants;
import by.botyanov.library.web.ParamConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class RedirectToEditBookCommand implements Command {
    private static final Logger LOG = Logger.getLogger(RedirectToEditBookCommand.class);

    private RedirectToEditBookCommand() {
    }

    private static class RedirectToEditBookCommandHolder {
        private final static RedirectToEditBookCommand instance = new RedirectToEditBookCommand();
    }

    public static RedirectToEditBookCommand getInstance() {
        return RedirectToEditBookCommandHolder.instance;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException{
        long bookId = Integer.parseInt(request.getParameter(ParamConstants.BOOK_ID));
        Book book;
        ArrayList<Author> foundAuthors;
        ArrayList<Book> foundBook;
        try {
            foundAuthors = (ArrayList<Author>)AuthorService.getAll();
            foundBook = (ArrayList<Book>)BookService.search("");
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        try {
            book = BookService.find(bookId);
            LOG.debug(book);
        } catch (ServiceException e) {
            LOG.debug(e.getMessage());
            throw new CommandException(e);
        }
        request.setAttribute(ParamConstants.AUTHORS_LIST, foundAuthors);
        request.setAttribute(ParamConstants.BOOK_LIST, foundBook);
        request.setAttribute(ParamConstants.BOOK, book);
        return PageConstants.EDIT_BOOK;
    }
}



