package by.botyanov.library.dao.pool;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    public static final int CONNECTION_NUMBER = 20;
    public static final int MIN_CONNECTION_NUMBER = 16;
    private static final Logger LOG = Logger.getLogger(ConnectionPool.class);
    private static ConnectionPool instance;
    private static AtomicBoolean instanceCreated = new AtomicBoolean();
    private static ReentrantLock lock = new ReentrantLock();
    private static ArrayBlockingQueue<ProxyConnection> connectionQueue;

    private ConnectionPool() {
    }

    public static ConnectionPool getInstance() {
        if (!instanceCreated.get()) {
            try {
                lock.lock();
                if (instance == null) {
                    instance = new ConnectionPool();
                    openConnections();
                    if (connectionQueue.size() < MIN_CONNECTION_NUMBER) {
                        throw new RuntimeException("Not enough connections in pool");
                    }
                    instanceCreated.set(true);
                    LOG.info("Instance of Connection Pool created");
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public ProxyConnection takeConnection() {
        ProxyConnection connection = null;
        try {
            connection = connectionQueue.take();
        } catch (InterruptedException e) {
            LOG.error(e);
        }
        return connection;
    }

    public void closeConnection(ProxyConnection connection) {
        try {
            if (!connection.getAutoCommit()) {
                connection.setAutoCommit(true);
            }
            connectionQueue.put(connection);
        } catch (SQLException | InterruptedException e) {
            LOG.error(e);
        }
    }

    public void closeAllConnections() {
        connectionQueue.forEach(ProxyConnection::realCloseConnection);
    }

    private static void openConnections() {
        ProxyConnection proxyConnection;
        try {
            ResourceBundle prop = ResourceBundle.getBundle("resources.db");

            String host = prop.getString("db.host");
            String dbName = prop.getString("db.name");
            String username = prop.getString("db.username");
            String password = prop.getString("db.password");
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());

            connectionQueue = new ArrayBlockingQueue<>(CONNECTION_NUMBER);
            for (int i = 0; i < CONNECTION_NUMBER; i++) {
                Connection connection = DriverManager.getConnection(host + dbName, username, password);
                proxyConnection = new ProxyConnection(connection);
                connectionQueue.offer(proxyConnection);
            }
            LOG.info(connectionQueue.size() + " connections created");
        } catch (SQLException e) {
            LOG.error(e);
        }
    }
}